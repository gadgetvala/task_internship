// Everything here is just for referance you can create new or modify it

// firebase configure
const firebaseConfig = {
	apiKey: 'YOUR_API_KEY',
	authDomain: 'YOUR_AUTH_DOMAIN',
	databaseURL: 'YOUR_FIRESTONE_URL',
	projectId: 'YOUR_PROJEC_ID',
	storageBucket: 'YOUR_FIREBASE_STORAGE',
	appId: 'YOUR_APP_ID'
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const storage = firebase.storage().ref();

async function submit() {
	let uid = document.getElementById('uidField');
	let name = document.getElementById('nameField');
	let image = document.getElementById('imageField');
	let recipeInstruction = document.getElementById('recipe_Instruction');
	let recipeExtendedIngredients = document.getElementById('recipe_extendedIngredients');

	let filename = `${Date.now()}-${image.files[0].name}`;

	let sendRecipe = {
		name: name.value,
		user: uid.value,
		image: downloadURL,
		recipe: {
			analyzedInstructions: [
				{
					steps: recipeInstruction.value
				}
			],
			extendedIngredients: getJSONbyString(recipeExtendedIngredients.value),
			instructions: recipeInstruction.value
		}
	};

	db.collection('test')
		.add(sendRecipe)
		.then((docref) => {
			console.log('Recipe creaeted SuccesFully!');
		})
		.catch((err) => {
			console.log('Recipe is not created some error occor ,', error);
		});
}
